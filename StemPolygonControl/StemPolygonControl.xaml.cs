﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StemPolygonControl
{
    /// <summary>
    /// Interaction logic for StemPolygonControl.xaml
    /// </summary>
    public partial class StemPolygonControl : UserControl
    {
        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }
        public StemPolygonControl()
        {
            InitializeDeploymentKey();
            InitializeComponent();
            InitStartParams();

            //GenerateData();
            GenerateDefaultData();
        }

        private void InitStartParams()
        {
            GlobalNumberOfAntens = 16;
        }

        private int _GlobalNumberOfAntens;
        public int GlobalNumberOfAntens
        {
            get { return _GlobalNumberOfAntens; }
            set
            {
                if (value >= 0 && _GlobalNumberOfAntens != value)
                {
                    _GlobalNumberOfAntens = value;
                    ReCalcRange();
                }
            }
        }

        public void GenerateData()
        {
            Random r = new Random();

            polygonSeries.Clear();

            for (int i = 1; i < _GlobalNumberOfAntens + 1; i++)
            {
                // Add several polygons.
                AddPolygon(i.ToString(), ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(i, r.NextDouble() * 255d), false);
            }

            VXY.PolygonSeries = polygonSeries;
        }

        PolygonSeriesCollection polygonSeries = new PolygonSeriesCollection();
        Color colorFirst = Color.FromArgb(255, 255, 192, 0);
        Color colorLast = Color.FromArgb(255, 255, 94, 0);
        private void GenerateDefaultData()
        {
            polygonSeries.Clear();

            for (int i = 1; i < _GlobalNumberOfAntens + 1; i++)
            {
                // Add several polygons.
                AddPolygon(i.ToString(), ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(i), false);
            }

            VXY.PolygonSeries = polygonSeries;
        }

        private PointDouble2D[] GeneratePointDouble2D(double x, double y=0)
        {
            return new PointDouble2D[] {
                new PointDouble2D(x - 1/3d, 0),
                new PointDouble2D(x - 1/3d, y),
                new PointDouble2D(x + 1/3d, y),
                new PointDouble2D(x + 1/3d, 0)
            };
        }

        /// <summary>
        /// Add Polygon.
        /// </summary>
        /// <param name="title">polygon title text</param>
        /// <param name="color">polygon color</param>
        /// <param name="edgePoints">polygon edge points</param>
        /// <param name="intersectionsAllowed">Intersections allowed. Note that having lots of complex polygons with IntersectionsEnabled = True will degrade the performance a lot.</param>
        private void AddPolygon(string title, Color color, PointDouble2D[] edgePoints, bool intersectionsAllowed)
        {
            PolygonSeries series = new PolygonSeries();
            series.MouseHighlight =  MouseOverHighlight.None;
            series.MouseInteraction = false;
            series.Fill.GradientFill = GradientFill.Solid;
            series.Fill.Color = color;
            series.Title.Visible = true;
            series.Title.Color = Color.FromArgb(255, 30, 30, 30);
            series.Title.Text = title;
            series.Title.Shadow.Style = TextShadowStyle.DropShadow;
            series.Points = edgePoints;
            series.IntersectionsAllowed = intersectionsAllowed;
            polygonSeries.Add(series);
        }

        public void DisplayData(byte[] Values)
        {
            if (Values.Count() == _GlobalNumberOfAntens)
            {
                double[] dValues = Values.Select(x => (double)x).ToArray<double>();
                SetData(dValues);
            }
        }
        public void DisplayData(double[] Values)
        {
            if (Values.Count() == _GlobalNumberOfAntens)
                SetData(Values);
        }

        private void SetData(double[] Values)
        {
            polygonSeries.Clear();

            for (int i = 1; i < _GlobalNumberOfAntens + 1; i++)
            {
                // Add several polygons.
                AddPolygon(i.ToString(), ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(i, Values[i-1]), false);
            }

            VXY.PolygonSeries = polygonSeries;
        }
       

        private void ReCalcRange()
        {
            xAxis.SetRange(0, _GlobalNumberOfAntens + 1);

            GenerateCustomTicks(ref xAxis);
        }

        private void GenerateCustomTicks(ref AxisX axisX)
        {
            axisX.CustomTicks.Clear();
            for (int i = 1; i < _GlobalNumberOfAntens + 1; i++)
            {
                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    i,
                    i.ToString("G0"),
                    6,
                    true,
                    axisX.MajorDivTickStyle.Color,
                    CustomTickStyle.Tick);

                axisX.CustomTicks.Add(cTick);
            }

            //Allow showing the custom tick strings
            axisX.CustomTicksEnabled = true;

            axisX.InvalidateCustomTicks();
        }

        private void xAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            GenerateCustomTicks(ref xAxis);
        }


        
          

      
    }
}
