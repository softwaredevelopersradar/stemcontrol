﻿#pragma checksum "..\..\StemControl.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "E5F8635D3307C78DFCB644ECBC7A8C0859861F26FE13F554F02F93802DFEB759"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.ChartManager;
using Arction.Wpf.SemibindableCharting.EventMarkers;
using Arction.Wpf.SemibindableCharting.Maps;
using Arction.Wpf.SemibindableCharting.OverlayElements;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.SeriesPolar;
using Arction.Wpf.SemibindableCharting.SeriesRound;
using Arction.Wpf.SemibindableCharting.SeriesSmith;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Titles;
using Arction.Wpf.SemibindableCharting.TypeConverters;
using Arction.Wpf.SemibindableCharting.Views;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using Arction.Wpf.SemibindableCharting.Views.ViewPie3D;
using Arction.Wpf.SemibindableCharting.Views.ViewPolar;
using Arction.Wpf.SemibindableCharting.Views.ViewRound;
using Arction.Wpf.SemibindableCharting.Views.ViewSmith;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using StemControl;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StemControl {
    
    
    /// <summary>
    /// StemControl
    /// </summary>
    public partial class StemControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\StemControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Arction.Wpf.SemibindableCharting.LightningChartUltimate stemChart;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\StemControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Arction.Wpf.SemibindableCharting.Axes.AxisX xAxis;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\StemControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Arction.Wpf.SemibindableCharting.Axes.AxisY yAxes;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\StemControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Arction.Wpf.SemibindableCharting.SeriesXY.PointLineSeries PLS;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\StemControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Arction.Wpf.SemibindableCharting.SeriesXY.LineCollection LC;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StemControl;component/stemcontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\StemControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.stemChart = ((Arction.Wpf.SemibindableCharting.LightningChartUltimate)(target));
            return;
            case 2:
            this.xAxis = ((Arction.Wpf.SemibindableCharting.Axes.AxisX)(target));
            
            #line 22 "..\..\StemControl.xaml"
            this.xAxis.RangeChanged += new Arction.Wpf.SemibindableCharting.Axes.AxisBase.RangeChangedEventHandler(this.xAxis_RangeChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.yAxes = ((Arction.Wpf.SemibindableCharting.Axes.AxisY)(target));
            return;
            case 4:
            this.PLS = ((Arction.Wpf.SemibindableCharting.SeriesXY.PointLineSeries)(target));
            return;
            case 5:
            this.LC = ((Arction.Wpf.SemibindableCharting.SeriesXY.LineCollection)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

