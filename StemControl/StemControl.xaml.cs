﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StemControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class StemControl : UserControl
    {
        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        public StemControl()
        {
            InitializeDeploymentKey();
            InitializeComponent();

            InitStartParams();

            //GenerateData();
            GenerateDefaultData();
        }

        private void InitStartParams()
        {
            GlobalNumberOfAntens = 16;
        }
       
        private int _GlobalNumberOfAntens;
        public int GlobalNumberOfAntens
        {
            get { return _GlobalNumberOfAntens; }
            set
            {
                if (value >=0 && _GlobalNumberOfAntens != value)
                {
                    _GlobalNumberOfAntens = value;
                    ReCalcRange();
                }
            }
        }

        private void ReCalcRange()
        {
            xAxis.SetRange(0, _GlobalNumberOfAntens + 1);

            GenerateCustomTicks(ref xAxis);
        }

        private void GenerateCustomTicks(ref AxisX axisX)
        {
            axisX.CustomTicks.Clear();
            for (int i = 1; i < _GlobalNumberOfAntens +1; i++)
            {
                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    i,
                    i.ToString("G0"),
                    6,
                    true,
                    axisX.MajorDivTickStyle.Color,
                    CustomTickStyle.Tick);

                axisX.CustomTicks.Add(cTick);
            }

            //Allow showing the custom tick strings
            axisX.CustomTicksEnabled = true;

            axisX.InvalidateCustomTicks();
        }

        public void DisplayData(byte[] Values)
        {
            if (Values.Count() == _GlobalNumberOfAntens)
            {
                double[] dValues = Values.Select(x => (double)x).ToArray<double>();
                SetData(dValues);
            }
        }
        public void DisplayData(double[] Values)
        {
            if (Values.Count() == _GlobalNumberOfAntens)
                SetData(Values);
        }

        private void SetData(double [] Values)
        {
            int pointCount = _GlobalNumberOfAntens + 1;

            SeriesPoint[] points = new SeriesPoint[pointCount];

            points[0].X = 0;
            points[0].Y = -10;

            for (int i = 1; i < pointCount; i++)
            {
                points[i].X = (double)i;
                points[i].Y = Values[i - 1];
            }

            PLS.Points = points;

            SegmentLine[] lines = new SegmentLine[pointCount];
            for (int i = 1; i < pointCount; i++)
            {
                lines[i].AX = points[i].X;
                lines[i].AY = points[i].Y;
                lines[i].BX = points[i].X;
                lines[i].BY = 0;
            }

            LC.Lines = lines;
        }

        public void GenerateData()
        {
            Random r = new Random();

            int pointCount = _GlobalNumberOfAntens + 1;

            SeriesPoint[] points = new SeriesPoint[pointCount];

            points[0].X = 0;
            points[0].Y = -10;

            for (int i = 1; i < pointCount; i++)
            {
                points[i].X = (double)i;
                points[i].Y = r.NextDouble() * 255d;
            }

            PLS.Points = points;

            SegmentLine[] lines = new SegmentLine[pointCount];
            for (int i = 1; i < pointCount; i++)
            {
                lines[i].AX = points[i].X;
                lines[i].AY = points[i].Y;
                lines[i].BX = points[i].X;
                lines[i].BY = 0;
            }

            LC.Lines = lines;
        }

        private void GenerateDefaultData()
        {
            int pointCount = _GlobalNumberOfAntens + 1;

            SeriesPoint[] points = new SeriesPoint[pointCount];

            points[0].X = 0;
            points[0].Y = -10;

            for (int i = 1; i < pointCount; i++)
            {
                points[i].X = (double)i;
                points[i].Y = 0;
            }

            PLS.Points = points;

            SegmentLine[] lines = new SegmentLine[pointCount];
            for (int i = 1; i < pointCount; i++)
            {
                lines[i].AX = points[i].X;
                lines[i].AY = points[i].Y;
                lines[i].BX = points[i].X;
                lines[i].BY = 0;
            }

            LC.Lines = lines;
        }

        private void xAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            GenerateCustomTicks(ref xAxis);
        }
    }
}
